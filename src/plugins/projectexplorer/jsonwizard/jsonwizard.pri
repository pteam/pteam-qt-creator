HEADERS += $$PWD/jsonfieldpage.h \
    $$PWD/jsonfilepage.h \
    $$PWD/jsonsummarypage.h \
    $$PWD/jsonwizard.h \
    $$PWD/jsonwizardexpander.h \
    $$PWD/jsonwizardfactory.h \
    $$PWD/jsonwizardfilegenerator.h \
    $$PWD/jsonwizardgeneratorfactory.h \
    $$PWD/jsonwizardpagefactory.h \
    $$PWD/jsonwizardpagefactory_p.h

SOURCES += $$PWD/jsonfieldpage.cpp \
    $$PWD/jsonfilepage.cpp \
    $$PWD/jsonsummarypage.cpp \
    $$PWD/jsonwizard.cpp \
    $$PWD/jsonwizardexpander.cpp \
    $$PWD/jsonwizardfactory.cpp \
    $$PWD/jsonwizardfilegenerator.cpp \
    $$PWD/jsonwizardgeneratorfactory.cpp \
    $$PWD/jsonwizardpagefactory.cpp \
    $$PWD/jsonwizardpagefactory_p.cpp
